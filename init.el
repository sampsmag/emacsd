(prog1 "prepare package repository"
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") 
  (custom-set-variables
    '(package-archives '(("org"       . "https://orgmode.org/elpa/")
                         ("melpa"     . "https://melpa.org/packages/")
                         ("gnu"       . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/"))))
  (package-initialize))

(prog1 "install leaf"
  (unless (package-installed-p 'leaf)
    (package-refresh-contents)
    (package-install 'leaf))

  (leaf leaf-keywords
        :ensure t
        :config (leaf-keywords-init)))

(leaf org :ensure t)
(setq samps-settings-org-file "settings.org")
(setq samps-settings-org-path
      (expand-file-name
        (concat user-emacs-directory samps-settings-org-file)))
(org-babel-load-file samps-settings-org-path)

(setq org-image-actual-width nil)
